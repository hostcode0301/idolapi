using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using idolapi.DB.DTOs;
using idolapi.DB.Models;
using idolapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace idolapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IdolController : ControllerBase
    {
        private readonly IIdolService _idolService;
        private readonly IMapper _mapper;

        public IdolController(IIdolService idolService, IMapper mapper)
        {
            _idolService = idolService;
            _mapper = mapper;
        }

        /// <summary>
        /// List all idols in database
        /// </summary>
        /// <returns>200: List of idol</returns>
        [HttpGet]
        public async Task<ActionResult<List<IdolDTO>>> ListAllIdol()
        {
            return Ok();
        }

        /// <summary>
        /// Get an idol using idol Id
        /// </summary>
        /// <param name="idolId">Id of idol to get</param>
        /// <returns>200: Idol / 404: Not found</returns>
        [HttpGet("{idolId:int}")]
        public async Task<ActionResult<IdolDTO>> GetIdol(int idolId)
        {
            return Ok();
        }

        /// <summary>
        /// Add new idol to database
        /// </summary>
        /// <param name="idolInput"></param>
        /// <returns>201: Created / 400: Model fail / 409: Existed</returns>
        [HttpPost]
        public async Task<ActionResult> CreateIdol([FromBody] IdolInput idolInput)
        {
            return Created("", new ResponseDTO
            {
                Status = 201,
                Message = "Insert done"
            });
        }

        /// <summary>
        /// Delete a idol with selected id
        /// </summary>
        /// <param name="idolId">If of idol want to delete</param>
        /// <returns>200: Deleted / 404: Not found</returns>
        [HttpDelete("{idolId:int}")]
        public async Task<ActionResult> DeleteIdol(int idolId)
        {
            return Ok();
        }

        /// <summary>
        /// Update an existed Idol with new data
        /// </summary>
        /// <param name="idolInput">New idol data</param>
        /// <param name="idolId">Id of selected Idol</param>
        /// <returns>200: Updated / 404: Not found / 409: Bad request</returns>
        [HttpPut("{idolId:int}")]
        public async Task<ActionResult> UpdateIdol([FromBody] IdolInput idolInput, int idolId)
        {
            return Ok();
        }
    }
}