using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using idolapi.DB;
using idolapi.DB.Models;
using Microsoft.EntityFrameworkCore;

namespace idolapi.Services
{
    public class IdolService : IIdolService
    {
        private DataContext _dataContext;

        public IdolService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <summary>
        /// List all idol in database
        /// </summary>
        /// <returns>List of idols</returns>
        public async Task<List<Idol>> ListAllIdol()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Get idol method using id
        /// </summary>
        /// <param name="id">Idol id</param>
        /// <returns>Idol data</returns>
        public async Task<Idol> GetIdolById(int id)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Insert new idol to database
        /// </summary>
        /// <param name="idol">Idol input data</param>
        /// <returns>-1: existed / 0: fail / 1: done</returns>
        public async Task<int> InsertIdol(Idol idol)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Delete a idol in database
        /// </summary>
        /// <param name="id">Idol id</param>
        /// <returns>-1: not found / 0: fail / 1: done</returns>
        public async Task<int> DeleteIdol(int id)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Update an idol with new input data
        /// </summary>
        /// <param name="idol">New idol data</param>
        /// <param name="id">The id to update</param>
        /// <returns>-1: not found / 0: fail / 1: done</returns>
        public async Task<int> UpdateIdol(Idol idol, int id)
        {
            throw new System.NotImplementedException();
        }
    }
}