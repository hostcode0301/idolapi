# Idol Project

Basic ASP.NET Core API architecture project

## Installation

Use dotnet cli to install all packages in Idol Project.

```bash
dotnet restore idolapi.csproj
```

## Usage

Change code in two folder to make Idol Controller and Service work:

-   Services: interact with database and get data
-   Controllers: validate request, call Services to get data and return JSON data

## Function detail

### Authentication:

-   (Anonymous) Register
-   (Anonymous) Login

### Idol:

-   (Anonymous) List all
-   (Anonymous) Get by IdolId
-   (Authorize) Insert new Idol
-   (Authorize) Update Idol
-   (Authorize) Delete Idol

### Seach:

_(If you want to write it yourself)_

-   (Anonymous) Seach by filter (name, age, 3-round measurement, height)
